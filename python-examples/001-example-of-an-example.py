"""
Example of a sphinx gallery example
===================================

This example introduces the sphinx gallery features. The first thing sphinx
gallery requires is a module docstring, such as this one. To understand the
formatting, try comparing the script form of this page with the rendered HTML
page. 

Sphinx gallery allows you to create runnable examples -- python scripts -- which
are turned into an HTML gallery. The sphinx gallery documentation can be found
here:

https://sphinx-gallery.github.io/stable/index.html

When the documentation is built, the examples are executed during the continuous
integration, and the output is stored in HTML pages. These HTML pages are made
accessible online.

Note that documentation and comments are written in restructured text (rST). It
is a very flexible markup language, and it the standard for writing
documentation for Python programs. Its documentation can be found here:

https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html

All the rST is placed within comments (or module docstrings). This means the file
remains a valid executable Python script.
"""

1 + 1

###############################################################################
# Block splitters, as seen above, consist of 79 ``#`` symbols.
# All code within a block is executed, and the result will be shown at the end
# of the block.

import matplotlib.pyplot as plt
plt.plot([1, 2, 3, 4])
