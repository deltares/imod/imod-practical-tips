iMOD practical tips
===================

This repository is a collection of tips and handy utilities.
The rendered form can be found here:

https://deltares.gitlab.io/imod/imod-practical-tips/

Contributing
------------

The easiest way of contributing consists of adding e.g. a python example. This
can be added to the ``python-examples`` directory. You can find an example in
there, ``001-example-of-an-example.py`` which shows the basic principle.

To see whether your example behaves appropriately, make sure you have the
required dependencies installed as defined in ``environment.yml`` (or create a
new environment).

After including the example and installing all necessary dependencies, you can
generate the pages by navigating to the ``docs`` directory and running

```
make html
```

This will call on the sphinx documentation generator to render the files to
HTML. You can view the rendered files by opening ``_build/html/index.html``. If
it's looking satisfactory, commit and push to this repository. During the
continuous integration step, your example will be executed, rendered, and made
accessible online at the address mentioned above. 
