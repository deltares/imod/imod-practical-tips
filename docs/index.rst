imod-practical-tips
===================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   snakemake_tips
   modflow_parallel
   git_dvc
   powershell
   handling_messy_data
   sharing_conda_environments

* :doc:`snakemake_tips`
* :doc:`modflow_parallel`
* :doc:`git_dvc`
* :doc:`powershell`
* :doc:`handling_messy_data`
* :doc:`sharing_conda_environments`
* :doc:`how_to_look_like_a_pro`

Examples
========

.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Examples
   gallery_examples/index

* :doc:`gallery_examples/index`

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
