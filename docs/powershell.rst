.. toctree::
    :maxdepth: 2 :hidden:

Fixing the Powershell profile error
===================================

Your computer might throw you the following error in Windows PowerShell. 
This is annoying, because it prevents the 
Visual Studio Code terminal (which uses PowerShell) to use conda.

.. code-block:: console

    File C:\Users\Herman\Documents\WindowsPowerShell\profile.ps1 cannot be loaded because the
    execution of scripts is disabled on this system. Please see "get-help about_signing" for
    more details.
    At line:1 char:2
    + . <<<<  'C:\Users\Herman\Documents\WindowsPowerShell\profile.ps1'
        + CategoryInfo          : NotSpecified: (:) [], PSSecurityException
        + FullyQualifiedErrorId : RuntimeException

In order to solve this, it is recommended to run the following command:

.. code-block:: console

    Set-ExecutionPolicy RemoteSigned -Scope CurrentUser

For more information, see `this link <https://stackoverflow.com/questions/4647429/powershell-on-windows-7-set-executionpolicy-for-regular-users/4732316>`_.