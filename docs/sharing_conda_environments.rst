.. toctree::
   :maxdepth: 2
   :hidden:


******************************
Sharing ``conda`` environments
******************************

The package manager ``conda`` allows you to manage environments with packages
with shared dependencies, to avoid *dependency hell*. It is highly recommended
to use either ``conda`` or its faster implementation ``mamba``. For a writeup on
how to carefully install packages with ``conda``, see `this section in the iMOD
Python documentation. 
<https://deltares.gitlab.io/imod/imod-python/faq/python.html>`_ One of the great
features of ``conda`` is that it allows you to share Python environments with
others.


To machines with internet
#########################

Sharing ``conda`` environments with others connected to the internet is easy.
Fist, activate your environment, in this example named ``my_environment``:

.. code-block::

    conda activate my_environment

Next, you can call the following command on you machine:

.. code-block::

    conda env export > environment.yml

This exports the list of packages in your environment with their specific
versions to a ``.yml`` file. This is a very lightweight textfile, which you can
easily copy to the other machine and call:

.. code-block::

    conda env create -f environment.yml

to recreate the environment. With older environments, however, this will likely
not work, because older versions of packages are usually removed from the most
commonly used ``conda-forge`` channel after a period of time. In this case, you
can do two things.

First, you can try exporting your environment with the following command:

.. code-block::

    conda env export --from-history > environment.yml

This will only list the packages you explicitly provided via a command or
``.yml`` file, and not the dependencies that came along with it. If you
specified a version number of a package during its installation, this is also
memorized. This will not exactly recreate the environment, but is often
sufficient.

Second, you could follow the instructions in the next section to get an exact
copy.

To machines without internet
############################

The easy (but untested) way
**************************

Intstall ``conda-pack`` on your machine. `See its documnetation for a complete
instruction. <https://conda.github.io/conda-pack/index.html>`_

.. code-block::

    conda activate base
    conda install -c conda-forge conda-pack

To pack you environment:

.. code-block::

    conda pack --name my_environment --output environment.zip

This will pack your environment to a zip file, which you can copy to the destination machine.
Unpack the zipfile at the destination machine.

Activate the environment (TODO: Figure out how to do this for Windows), and 
then call 

.. code-block::

    conda-unpack

.. warning::

    We did not have the opportunity to test this, so we can only state that this
    SHOULD work based on what people write on the internet.

The hard way
************

Sharing environments to machines protected from the horrors of the internet,
e.g. computational servers, is more difficult. First, activate your environment
(still named ``my_environment`` in this example):

.. code-block::

    conda activate my_environment

Next, call:

.. code-block::

    conda info

This will list the active location of the environment under ``active env location``, for example:

.. code-block::

    active environment : imod
    active env location : C:\Users\your_name\miniconda3\envs\my_environment

You can zip the directory printed under ``active env location`` with a program of your choice.
For example, with Powershell, you can run:

.. code-block::

    Compress-Archive -LiteralPath 'C:\Users\your_name\miniconda3\envs\my_environment' -DestinationPath 'C:\Users\your_name\my_environment.zip'

Copy the zip file to the destination machine's conda installation. On the LHM
server, this folder is named ``e:\tools\Miniconda3\envs``. 

Now comes the painful part, you have to search your files manually to find any
traces of hardcoded paths which need to be updated to the new machine. In our
case, the following files needed their paths fixed to get a working environment.

.. code-block::

    Scripts\snakemake.exe
    Scripts\yapf-diff
    Scripts\qta-browser
    Scripts\qdarkstyle
    Scripts\pytest-benchmark
    Scripts\isort-identify-imports
    Scripts\deactivate
    Scripts\blackd
    Scripts\activate
    qt.conf
